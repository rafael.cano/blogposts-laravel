<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreAuthor;
use App\Models\Author;
use App\Models\Profile;
use App\Notifications\AuthorCreated;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('authors.index', ['authors' => Author::with('profile')->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('authors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAuthor $request)
    {
        $validated = $request->validated();
        $author = new Author();
        $profile = new Profile();
        $author->name = $validated['authorName'];
        $profile->email = $validated['authorEmail'];
        $author->save();
        $author->profile()->save($profile);

        $user = auth()->user();
        $user->prefers_sms = true;
        $channel = $user->prefers_sms ? 'SMS' : 'mail';
        $user->notify(new AuthorCreated($author));

        $request->session()->flash('status', 'New Author ' . $author->name . ' has been created and notification by ' . $channel . ' has been sent!');
        return redirect()->route('authors.show', ['author' => $author->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('authors.show', ['author' => Author::findOrFail($id)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('authors.update', ['author' => Author::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $author = Author::findOrFail($id);
        $profile = Profile::Where('author_id', $id)->first();
        $validated = $request->validated();
        $author->fill($validated);
        $author->name = $validated['authorName'];
        $profile->email = $validated['email'];
        return redirect()->route('authors.show', ['author' => $author->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $author = Author::findOrFail($id);
        $author->delete();
        session()->flash('status', 'The Author' . $author->name . 'was deleted!');
        return redirect()->route('authors.index');
    }
}
