<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBlogPost;
use App\Mail\BlogPostCreated;
use App\Models\BlogPost;
use App\Models\Image;
use Carbon\Carbon;
use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class BlogPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth')->only(['create']);
    }
    public function index()
    {
        // return view('posts.index', ['posts' => $this->blogposts]);
        // return view('posts.index', ['posts' => BlogPost::all()]);
        $posts = BlogPost::withCount(['comments', 'comments as new_comments' => function (Builder $query) {
            $query->where('created_at', '>', Carbon::now()->subMonths(6));
        },])->with('user')->get();
        return view('posts.index', ['posts' => $posts]);
        //return view('posts.index', ['posts' => BlogPost::withCount(['comments','comments as new_comments'=>'comments as new_comments'=> function (Builder $query){},])->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBlogPost $request)
    {
        $validated = $request->validated();
        $blogpost = new BlogPost();
        $blogpost->blogPostTitle = $validated['blogPostTitle'];
        $blogpost->blogPostContent = $validated['blogPostContent'];
        $blogpost->blogPostIsHighlight = $request['blogPostHighlight'] == 'on' ? 1 : 0;

        $user = $request->user();
        $user->blogposts()->save($blogpost);

        if ($request->hasFile('blogPostImage')) {
            $path = $request->file('blogPostImage')->store('blogPostImages');
            $blogpost->image()->save(Image::create(['imagePath' => $path]));
        }

        $request->session()->flash('status', 'The Blog Post was created!');

        Mail::to($request->user())->send(new BlogPostCreated($blogpost));

        return redirect()->route('blogposts.show', ['blogpost' => $blogpost->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // abort_if(!isset($this->blogposts[$id]), 404);
        // return view('posts.show', ['post' => $this->blogposts[$id]]);
        $post = BlogPost::withCount('comments')->findOrFail($id);
        return view('posts.show', ['post' => $post]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('posts.update', ['post' => BlogPost::findOrFail($id)]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreBlogPost $request, $id)
    {
        $user = $request->user();
        if ($user->hasRole('Writer')) {
            abort(403);
        } else if ($user->hasRole('Admin') && $user->id != BlogPost::findOrFail($id)->user_id) {
            abort(403);
        }
        $blogpost = BlogPost::findOrFail($id);
        $validated = $request->validated();

        $blogpost->fill($validated);
        $blogpost->blogPostIsHighLight = $request['blogPostHighlight'] == 'on' ? 1 : 0;

        if ($request->hasFile('blogPostImage')) {
            $path = $request->file('blogPostImage')->store('blogPostImages');
            if ($blogpost->image) {
                Storage::delete($blogpost->image->imagePath);
                $blogpost->image->imagePath = $path;
                $blogpost->image->save();
            } else {
                $blogpost->image()->save(Image::create(['imagePath' => $path]));
            }
        }

        $request->session()->flash('status', 'The Blog Post was updated!');
        return redirect()->route('blogposts.show', ['blogpost' => $blogpost->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blogpost = BlogPost::findOrFail($id);
        $this->authorize('blogposts.delete', $blogpost);
        $comments = $blogpost->comments;
        foreach ($comments as $comment) {
            $comment->delete();
        }
        $blogpost->delete();
        session()->flash('status', 'The Blog Post with ID' . $id . ' was deleted.');
        return redirect()->route('blogposts.index');
    }
}
