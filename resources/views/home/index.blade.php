@extends('layout.app')
@section('title', 'Home page')
@section('content')
    {{-- <h5>GIPE 2022 - Let's start the endeavour and create a portal for the Colegio San Juan Apostol in Arequipa, Peru!</h5> --}}
    <div class="text-center">
        <p>
            <h5>GIPE 2022 - {{__('Hello!')}}
                <hr>
                {{__('This is our Home-Page!')}}
            </h5>
        </p>
    </div>
@endsection