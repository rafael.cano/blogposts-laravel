@extends('layout.app')
@section('title', 'Forbidden')
@section('content')
        <div class="card mx-auto" style="width: 50%;">
            <div class="tenor-gif-embed" data-postid="3530668" data-share-method="host" data-aspect-ratio="1.33333"
                data-width="100%"><a href="https://tenor.com/view/you-shall-not-pass-gandalf-lotr-gif-3530668">You Shall Not
                    Pass GIF</a>from <a href="https://tenor.com/search/you+shall+not+pass-gifs">You Shall Not Pass GIFs</a>
            </div>
            <script type="text/javascript" async src="https://tenor.com/embed.js"></script>
            <div class="card-body">
                <h5 class="card-title">Unauthorized!!</h5>
                <p class="card-text">You're not authorized to perform this action</p>
                <a href="/" class="btn btn-primary">Go home</a>
            </div>
        </div>
@endsection
