@extends('layout.app')
@section('title', 'Blog posts page')
@section('content')
    {{-- @if ($post['is_new'])
    <div>This is a new blog post</div>
        @elseif(!$post['is_new'])
        <div>This is an old blog post</div>
    @endif --}}
    <div>
        @if ($post['blogPostIsHighLight'])
            This is a highlited Blog Post!
        @elseif(!$post['blogPostIsHightlight'])
            This is not a highlighted Blog Post!
        @endif
    </div>
    <div class="d-flex flex-column align-items-center justify-content">
        @if ($post->image)
            <div class="bg-light">
                <img src="{{ $post->image->url() }}" alt="Blog Post Image" class="w-50 p-3">
                <h1>{{ $post['blogPostTitle'] }}</h1>
            </div>
            <p>{{ $post['blogPostContent'] }}</p>
        @else
            <h1>This Blog Post does not have an image!</h1>
            <h2>{{ $post['blogPostTitle'] }}</h2>
            <p>{{ $post['blogPostContent'] }}</p>
        @endif
        <div class="bg-secondary mw-100">
            {{-- This Blog Post has {{ $post['comments_count'] }} comments! --}}
            <h3>{{ __('blogPostComments') }} {{ trans_choice('numberOfBlogPostComments', $post['comments_count']) }}
            </h3>
        </div>
    </div>
@endsection
