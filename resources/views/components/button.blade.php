@csrf
<button type="submit" class="btn btn-{{ $class }}">{{ __($text) }}</button>