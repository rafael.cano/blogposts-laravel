<style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }

</style>

<p>
    Hi {{ $blogpost->user->name }},
</p>
<p>
    Thanks for your new Blog Post titled as <a
        href="{{ route('blogposts.show', [$blogpost->id]) }}">{{ $blogpost->blogPostTitle }}:</a>
    <hr>
</p>
<p>
    You wrote:
</p>
<p>
    {{ $blogpost->blogPostContent }}
    <hr>
</p>
@if ($blogpost->image)
    <p>
        Your uploaded image:
    </p>
    <p>
        <img src="{{ $blogpost->image->url() }}" alt="Blog Post Image" class="w-50 p-3">
    </p>
    <p>The uploaded file is also attached to this email!</p>
@endif
