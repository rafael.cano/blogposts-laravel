<?php

use App\Http\Controllers\AuthorController;
use App\Http\Controllers\BlogPostController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InternalAreaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
     // return view('welcome');
     // return "GIPE 2022 - Let's start the endeavour and create a portal for the Colegio San Juan Apostol in Arequipa, Peru!";
     // return view('home.index',[]);
// })->name('home.index');
// Route::get('/contact', function () {
    // return "This is the contact page!";
    // return view('home.contact',[]);
// })->name('contact.index');
Route::get('/', [HomeController::class,'home'])->name('home.index');
Route::get('/contact', [HomeController::class,'contact'])->name('contact.index');
// $posts = [
//     1 => [
//         'title' => 'Intro to laravel',
//         'content' => 'This is a short intro to laravel',
//         'is_new' => true
//     ],
//     2 => [
//         'title' => 'Intro to PHP',
//         'content' => 'This is a short intro to PHP',
//         'is_new' => false
//     ],
// ];
// Route::get('/posts', function () use ($posts) {
//     // return "This is the posts page!";
//     return view('posts.index',['posts' => $posts]);
// })->name('posts.index');
// Route::get('/posts/{id}', function ($id) use ($posts) {
//     // return "Blog Post {$id}";
//     abort_if(!isset($posts[$id]), 404);
//     return view('posts.show', ['post' => $posts[$id]]);
// })->name('posts.show');
Route::get('/recent-posts/{days_ago?}', function ($days_ago = 20) {
    return "Posts from {$days_ago} days ago";
})->name('posts.recent.index');
Route::resource('blogposts',BlogPostController::class);
Route::resource('authors',AuthorController::class);
Route::resource('comments',CommentController::class)->only(['store','create']);
Route::get('/home', [InternalAreaController::class, 'home'])->name('home.index');
Auth::routes();
