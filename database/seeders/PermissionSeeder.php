<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

use Spatie\Permission\PermissionRegistrar;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        Permission::create(['name' => 'list Blog posts']);
        Permission::create(['name' => 'create Blog posts']);
        Permission::create(['name' => 'store Blog posts']);
        Permission::create(['name' => 'show Blog posts']);
        Permission::create(['name' => 'edit Blog posts']);
        Permission::create(['name' => 'update Blog posts']);
        Permission::create(['name' => 'delete Blog posts']);

        $role1 = Role::create(['name' => 'Writer']);
        $role1->givePermissionTo('list Blog posts');
        $role1->givePermissionTo('create Blog posts');
        $role1->givePermissionTo('store Blog posts');
        $role1->givePermissionTo('show Blog posts');

        $role2 = Role::create(['name' => 'Admin']);
        $role2->givePermissionTo('list Blog posts');
        $role2->givePermissionTo('create Blog posts');
        $role2->givePermissionTo('store Blog posts');
        $role2->givePermissionTo('show Blog posts');
        $role2->givePermissionTo('edit Blog posts');
        $role2->givePermissionTo('update Blog posts');

        $role3 = Role::create(['name' => 'Super-Admin']);

        $user = User::factory()->create([
            'name' => 'Writer User',
            'email' => 'writer@gmail.com',
        ]);
        $user->save();
        $user->assignRole($role1);

        $user = User::factory()->create([
            'name' => 'Admin User',
            'email' => 'admin@gmail.com',
        ]);
        $user->save();
        $user->assignRole($role2);

        $user = User::factory()->create([
            'name' => 'Super Admin User',
            'email' => 'superadmin@gmail.com',
        ]);
        $user->save();
        $user->assignRole($role3);
    }
}
